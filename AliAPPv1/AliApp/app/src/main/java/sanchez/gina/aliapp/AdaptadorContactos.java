package sanchez.gina.aliapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by GinaMariela on 01/09/2016.
 */
public class AdaptadorContactos extends BaseAdapter {
    private Context context;
    ArrayList<Contact> arrayOfUsers;
    public void llenar( ArrayList<Contact> Users)
    {
        for (int x=0;x<Users.size();x++) {
            arrayOfUsers.add(Users.get(x));
        }
    }
    public AdaptadorContactos(Context context) {
        arrayOfUsers = new ArrayList<>();
        this.context = context;
    }

    @Override
    public int getCount() {
        return arrayOfUsers.size();
    }

    @Override
    public Contact getItem(int i) {
        return arrayOfUsers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return arrayOfUsers.get(i).id;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_item, viewGroup, false);
        }

        ImageView imagenCoche = (ImageView) view.findViewById(R.id.imagen_contacto);
        TextView nombreCoche = (TextView) view.findViewById(R.id.nombre_contacto);

        Contact user = getItem(position);

        if(user.photo!=null) {

            Uri r = Uri.parse(user.photo);

            //  imagenCoche.setImageURI(r);
            if (r != null) {
                //  Picasso.with(context).load(r).into(imagenCoche);
                Glide.with(context)
                        .load(Uri.parse(user.photo)).into(imagenCoche);
            }
        }
        nombreCoche.setText(user.name);

        return view;
    }
}
