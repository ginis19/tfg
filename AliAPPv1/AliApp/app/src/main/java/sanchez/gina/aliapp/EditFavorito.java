package sanchez.gina.aliapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import sanchez.gina.aliapp.Preferencias.SharedPreference;

public class EditFavorito extends AppCompatActivity  implements AdapterView.OnItemSelectedListener {
    BootstrapCircleThumbnail p1;
    Spinner spinner1Pos;
    Spinner spinner2Nombre;
    DBContactHelper mydb;
    ArrayList<Contact> arrayOfUsers;

    private SharedPreference sharedPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        sharedPreference = new SharedPreference();

        setContentView(R.layout.activity_edit_favorito);
        spinner1Pos = (Spinner) findViewById(R.id.spinnerPos);

        spinner2Nombre = (Spinner) findViewById(R.id.spinnerNombre);

         p1=(BootstrapCircleThumbnail) findViewById(R.id.verFavorito);

        spinner1Pos.setOnItemSelectedListener(this);
        spinner2Nombre.setOnItemSelectedListener(this);
        mydb = new DBContactHelper(this);
        arrayOfUsers = mydb.getAllContacts();
        loadSpinnerData();
    }

    private List<String> ObtenerNombres() {


        List<String> nombres = new ArrayList<String>();
        for (int i = 0; i < arrayOfUsers.size(); i++) {
            nombres.add(arrayOfUsers.get(i).name);

        }


        return nombres;
    }

    private boolean ponerFoto(String nombre)
    {
        List<String> nombres = new ArrayList<String>();
        for (int i = 0; i < arrayOfUsers.size(); i++) {
            if(nombre.equals(arrayOfUsers.get(i).name))
            {
                if(arrayOfUsers.get(i).photo!=null) {
                    Glide.with(this.getApplicationContext()).load(Uri.parse(arrayOfUsers.get(i).photo)).into(p1);

                    return true;
                }
            }

        }
        return false;
    }

    private void loadSpinnerData() {

        List<String> lables2 = new ArrayList<String>();
        lables2.add("1");
        lables2.add("2");
        lables2.add("3");

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, R.layout.spinner_item, lables2);
        List<String> nombres =ObtenerNombres();
        if(nombres.size()>0)
        {

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item,nombres);

            // Drop down layout style - list view with radio button

            dataAdapter.setDropDownViewResource(R.layout.spinner_item);


            dataAdapter2.setDropDownViewResource(R.layout.spinner_item);


            // attaching data adapter to spinner*/
            spinner2Nombre.setAdapter(dataAdapter);

            spinner1Pos.setAdapter(dataAdapter2);
        }else
        {

            Toast.makeText(this.getApplicationContext(),"POR FAVOR AÑADIR UN CONTACTO PRIMERO", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(),NewContact.class);
            startActivity(intent);
        }




    }
    public void actualizarFavorito(View v)
    {
        String text =  spinner1Pos.getSelectedItem().toString();

        String texto2= spinner2Nombre.getSelectedItem().toString();

        if(texto2!=null) {
            switch (text) {
                case ("1"):
                    sharedPreference.saveFav1(this.getApplicationContext(), texto2);

                    break;
                case ("2"):
                    sharedPreference.saveFav2(this.getApplicationContext(), texto2);
                    break;
                case ("3"):
                    sharedPreference.saveFav3(this.getApplicationContext(), texto2);
                    break;
                default:
                    Toast.makeText(this.getApplicationContext(), "NO SE HA ACTUALIZADO", Toast.LENGTH_SHORT).show();
            }
            Intent intent = new Intent(getApplicationContext(),FavoritosContactos.class);
            startActivity(intent);
            Toast.makeText(this.getApplicationContext(),"FAVORITO ACTUALIZADO", Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        // On selecting a spinner item
        String label = parent.getItemAtPosition(position).toString();

        Spinner spinner = (Spinner) parent;

        if(spinner.getId() == R.id.spinnerNombre)
        {
            ponerFoto(label);
           // Toast.makeText(parent.getContext(), "SPINER 1 You selected: " + label,  Toast.LENGTH_SHORT).show();
        }
        else if(spinner.getId() == R.id.spinnerPos)
        {

            //Toast.makeText(parent.getContext(), "SPINER 2 You selected: " + label, Toast.LENGTH_LONG).show();
        }
        // Showing selected spinner item

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }
}
