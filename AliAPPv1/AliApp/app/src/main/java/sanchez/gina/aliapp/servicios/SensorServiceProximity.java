package sanchez.gina.aliapp.servicios;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.display.DisplayManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.widget.Toast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import sanchez.gina.aliapp.MainActivity;
import sanchez.gina.aliapp.broadcast.SensorBroadcastReceiver;

public class SensorServiceProximity extends Service {
    public int counter=0;
    private static SensorManager sensorService;
    private Sensor sensor;


    public SensorServiceProximity(Context applicationContext) {
        super();
        Log.i("HERE", "here I am!");
    }

    public SensorServiceProximity() {
        super();
        Log.i("HERE", "here I am!");
    }
    @Override
    public void onCreate() {
        super.onCreate();
        sensorService = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorService.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        if (sensor != null) {
            Toast.makeText(this,"Servicio creado",Toast.LENGTH_SHORT).show();
            sensorService.registerListener(mySensorEventListener, sensor,SensorManager.SENSOR_DELAY_NORMAL);

        } else {
            Toast.makeText(this, "Sensor de proximidad no encontrado", Toast.LENGTH_LONG).show();
        }



    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        startTimer();

        Toast.makeText(this,"Servicio sensor proximidad arrancado" +startId, Toast.LENGTH_SHORT).show();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        sensorService.unregisterListener(mySensorEventListener);
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");
        Intent broadcastIntent = new Intent("RestartSensor");
        sendBroadcast(broadcastIntent);
        stoptimertask();
    }



    private Timer timer;
    private TimerTask timerTask;
    long oldTime=0;
    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 1000, 1000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  "+ (counter++));
            }
        };
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }


    private SensorEventListener mySensorEventListener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.values[0] == 0) {


               if(isScreenOn()==false) {
                   abrirpantalla2();
               }
            }
        }
    };


    private boolean isScreenOn () {
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= 20) {
            if (powerManager.isInteractive()) {
                return true;
            }
            return false;
           /* // I'm counting
            // STATE_DOZE, STATE_OFF, STATE_DOZE_SUSPENDED
            // all as "OFF"

            DisplayManager dm = (DisplayManager) getSystemService(Context.DISPLAY_SERVICE);
            for (Display display : dm.getDisplays ()) {
                if (display.getState () == Display.STATE_ON ||
                        display.getState () == Display.STATE_UNKNOWN) {
                    return true;
                }
            }

            return false;*/
        }

        if (powerManager.isScreenOn ()) {
            return true;
        }
        return false;
    }
    public void abrirpantalla2(){


       /* KeyguardManager.KeyguardLock key;
        KeyguardManager km = (KeyguardManager)getSystemService(KEYGUARD_SERVICE);

        //This is deprecated, but it is a simple way to disable the lockscreen in code
        key = km.newKeyguardLock("IN");
        key.disableKeyguard();*/
        Intent myIntent = new Intent(this.getApplicationContext(), MainActivity.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(myIntent);

    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
