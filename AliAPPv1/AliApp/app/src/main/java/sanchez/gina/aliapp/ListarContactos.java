package sanchez.gina.aliapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

public class ListarContactos extends AppCompatActivity  implements  AdapterView.OnItemClickListener {
    DBContactHelper mydb;

    private GridView gridView;
    private AdaptadorContactos adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_contactos);

        mydb = new DBContactHelper(this);
        ArrayList<Contact> arrayOfUsers = mydb.getAllContacts();

        gridView = (GridView) findViewById(R.id.grid);
        adaptador = new AdaptadorContactos(this);
        adaptador.llenar(arrayOfUsers);
        gridView.setAdapter(adaptador);

        gridView.setOnItemClickListener(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarListarContactos);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        Log.d("lista contactos+++", "onCreate() called");
    }

    public void ListarFavorito(View v)
    {
        Intent intent = new Intent(getApplicationContext(), FavoritosContactos.class);

        // Poner el Id de la imagen como extra en la intención
        //intent.putExtra("idContacto",itemValue);

        // Aquí pasaremos el parámetro de la intención creada previamente
        startActivity(intent);
    }
    public void listenAñadirContacto(final View view) {
        Intent intent = new Intent(getApplicationContext(), NewContact.class);

        // Poner el Id de la imagen como extra en la intención
        //intent.putExtra("idContacto",itemValue);

        // Aquí pasaremos el parámetro de la intención creada previamente
        startActivity(intent);
    }

    @Override

    /*

            lisContactos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
     */

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Contact item = (Contact) parent.getItemAtPosition(position);

        Intent intent = new Intent(getApplicationContext(), DetailContact.class);

        // Poner el Id de la imagen como extra en la intención
        intent.putExtra("idContacto", item);

        // Aquí pasaremos el parámetro de la intención creada previamente
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        adaptador.notifyDataSetChanged();
        Log.d("lista contactos+++", "onResume() called");
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.d("lista contactos+++", "onstart() called");
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d("lista contactos+++", "onPause() called");
    }

    @Override
    public void onStop() {

        super.onStop();
        Log.d("lista contactos+++", "onStop() called");

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.d("lista contactos+++", "onDestroy() called");

    }

}