package sanchez.gina.aliapp;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapProgressBar;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.Map;
import ai.api.AIConfiguration;
import ai.api.AIListener;
import ai.api.AIService;
import ai.api.GsonFactory;
import ai.api.model.AIError;
import ai.api.model.AIResponse;
import ai.api.model.Result;
import sanchez.gina.aliapp.Preferencias.SharedPreference;
import sanchez.gina.aliapp.servicios.SensorServiceProximity;

public class MainActivity extends AppCompatActivity implements GestureOverlayView.OnGesturePerformedListener  {
  //  DBContactHelper mydb ;
  BootstrapProgressBar bar;
  //  public static final String TAG = Auxilio.class.getName();
     private String TAG= "AppAli";
    Context ctx;
    private Gson gson = GsonFactory.getGson();
    private AIService aiService;
    private GestureLibrary mLibrary;
    Intent mServiceIntent;
    private SensorServiceProximity mSensorService;
    public Context getCtx() {
        return ctx;
    }
    MediaPlayer mMediaPlayer ;
    SharedPreference sharedPreference;
    void CancelarMensajeInicio() {

        sharedPreference.saveMensajeInicio(getCtx(),"si");
        Toast.makeText(this,"cancelado mensaje de inicio·",Toast.LENGTH_LONG).show();
    }

    public void MensajeInicio(){
        if(sharedPreference.getValueMensajeInicio(getCtx())==null) {

            mMediaPlayer.start();
            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {


                    aiService.startListening();

                }
            });
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ctx=this;
         bar =(BootstrapProgressBar) findViewById(R.id.barra);

        sharedPreference = new SharedPreference();
        mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.inicioapp);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(getApplicationContext(), Guia.class);
                startActivity(intent2);
            }
        });

        MensajeInicio();
        initServiceVoiceRecognition();
        initSensorService();
        cargarGesturas();
    }

    private void initSensorService()
    {
        mSensorService = new  SensorServiceProximity(getCtx());
        mServiceIntent = new Intent(getCtx(), mSensorService.getClass());
        if (!isMyServiceRunning(mSensorService.getClass())) {
            startService(mServiceIntent);
        }
    }

    private void initServiceVoiceRecognition() {

        final AIConfiguration config = new AIConfiguration("819d431dc64b46fe88086223054940da",
                    AIConfiguration.SupportedLanguages.Spanish,
                    AIConfiguration.RecognitionEngine.System);
        if (aiService != null) {
            aiService.pause();
        }

        aiService = AIService.getService(this, config);
        aiService.setListener( aimilistener);
    }

    @Override
    protected void onDestroy() {
        mMediaPlayer.stop();
        stopService(mServiceIntent);
        aiService.cancel();
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();
    }

    public void escuchar(View v)
    {

            aiService.startListening();

    }
/*
  <android.support.design.widget.AppBarLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:theme="@style/AppTheme.AppBarOverlay">

        <android.support.v7.widget.Toolbar
            android:id="@+id/toolbar"
            android:layout_width="match_parent"
            android:layout_height="?attr/actionBarSize"
            android:background="?attr/colorPrimary"
            app:popupTheme="@style/AppTheme.PopupOverlay"/>

    </android.support.design.widget.AppBarLayout>
  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @Override
    protected void onResume(){
       // mMediaPlayer.start();
        super.onResume();
    }
    @Override
    protected void onPause() {
        mMediaPlayer.pause();
        super.onPause();

        // use this method to disconnect from speech recognition service
        // Not destroying the SpeechRecognition object in onPause method would block other apps from using SpeechRecognition service
        /*if (aiService != null) {
            aiService.pause();
        }*/
    }

    public  void MostrarToast(String a)
    {
        Toast.makeText(this, a, Toast.LENGTH_SHORT)
                .show();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }

    private void menu(String accion)
    {
        switch (accion)
        {
            case "contactos":
                    Intent intent = new Intent(getApplicationContext(),ListarContactos.class);
                    startActivity(intent);
                break;
            case "emergencia":
                Intent intent1 = new Intent(getApplicationContext(),Auxilio.class);
                startActivity(intent1);
                break;
            case "favoritos":

                Intent intent2 = new Intent(getApplicationContext(),FavoritosContactos.class);
                startActivity(intent2);
                  break;
            case "CancelarMensajeInicio":
                CancelarMensajeInicio();
                break;
            default:
                break;

        }
    }
    public void cargarGesturas()
    {
        mLibrary = GestureLibraries.fromRawResource(this, R.raw.gesture);
        if (!mLibrary.load()) {
            finish();
        }
        GestureOverlayView gestures = (GestureOverlayView) findViewById(R.id.gestures);
        gestures.addOnGesturePerformedListener(this);
    }
private  AIListener aimilistener = new AIListener(){

    public void onResult(final AIResponse response) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Result result = response.getResult();

                // Get parameters
                String parameterString = "";
                if (result.getParameters() != null && !result.getParameters().isEmpty()) {
                    for (final Map.Entry<String, JsonElement> entry : result.getParameters().entrySet()) {
                        parameterString += "(" + entry.getKey() + ", " + entry.getValue() + ") ";
                    }
                }

                String h = "Query:" + result.getResolvedQuery() + "\nAction: " + result.getAction() + "\nParameters: " + parameterString;
                MostrarToast(result.getAction());
                if(result.getAction()!=null) {
                    menu(result.getAction());
                }/*if(result.getAction()==null)
                {
                    //llamar error
                }else {


                }*/
                /*

                if( result.getAction().equals("emergencia"))
                {
                    Intent intent = new Intent (getCtx(), Auxilio.class);
                    startActivityForResult(intent, 0);

                }
                //Toast.makeText(this,h, Toast.LENGTH_SHORT).show();*/


            }
        });
    }

    @Override
    public void onError(final AIError error) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                bar.setVisibility(View.GONE);
            }
        });
        //MostrarToast(error.toString());
        MostrarToast("Ha ocurrido un error");
    }
    @Override
    public void onListeningStarted() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                bar.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onListeningCanceled() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                bar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onListeningFinished() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
               bar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onAudioLevel(final float level) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                float positiveLevel = Math.abs(level);

                if (positiveLevel > 100) {
                    positiveLevel = 100;
                }
            }
        });

    }
};

    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        ArrayList<Prediction> predictions = mLibrary.recognize(gesture);
        double max=0.0;
        String name="";
        for (Prediction prediction : predictions) {
            if (prediction.score > 1.0) {
               /*Toast.makeText(this, prediction.name + "----"+ prediction.score, Toast.LENGTH_SHORT)
                        .show();*/
                if(prediction.score>max)
                {
                    name=prediction.name;
                    max= prediction.score;
                }
            }

        }
          menu(name);
    }

}
