package sanchez.gina.aliapp;
import java.text.Normalizer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.CallLog;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.ImageVideoWrapper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ai.api.AIConfiguration;
import ai.api.GsonFactory;
import ai.api.model.AIError;
import ai.api.model.AIResponse;
import ai.api.model.Metadata;
import ai.api.model.Result;
import ai.api.ui.AIButton;
import sanchez.gina.aliapp.Preferencias.SharedPreference;
import sanchez.gina.aliapp.servicios.ObtenerDireccionIntentService;


public class Auxilio extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ResultCallback<LocationSettingsResult> {

    private AIButton aiButton;
    private static final int MY_PERMISSION_ACCESS_COURSE_LOCATION = 0;
    private static final String TAG = "AuxilioActivity";

    private String dirrecionResuelta;

    //Constant used in the location settings dialog.

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    protected final static String KEY_LOCATION = "location";
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    protected LocationRequest mLocationRequest;

    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    protected LocationSettingsRequest mLocationSettingsRequest;
    AddressResultReceiver mResultReceiver;
    private SharedPreference sharedPreference;
    /**
     * Represents a geographical location.
     */
    protected Location mCurrentLocation;

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;
    Boolean errorVoz;
    Boolean clickContacto;



    /**
     * Time when the location was updated represented as a String.
     */
    protected String mLastUpdateTime;
    MediaPlayer mMediaPlayer ;

    ImageView im1 ;
    ImageView im2 ;
    ImageView im3 ;
    // Labels.
    DBContactHelper mydb;
    AIButton emergencia;
    int veces;
   Context context= this;
    public void openPregunta(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("FUE ATENDIDA SU EMERGENCIA?");

        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                MostrarToast("GRACIAS POR CONFIAR EN ALI APP");
            }
        });


        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                 superAlarma();
                // finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auxilio);
        mydb = new DBContactHelper(this);

       emergencia= (AIButton)findViewById(R.id.microfono);
       im1 =(ImageView)findViewById(R.id.eme1);
        im2 =(ImageView)findViewById(R.id.eme2);
        im3 =(ImageView)findViewById(R.id.eme3);

        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";
        sharedPreference = new SharedPreference();
        // Labels.
        // Update values using data stored in the Bundle.
        updateValuesFromBundle(savedInstanceState);
        ArrancarServiciosdeUbicacion();
        checkLocationSettings();

        mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.emergencia);
        leerFavoritos();
        MensajeInicio();
        ConfigurarAI();
        errorVoz=false;
        clickContacto=false;
        veces=0;
        phoneStart();
    }

    public void phoneStart()
    {
        // add PhoneStateListener
        PhoneCallListener phoneCallListener = new PhoneCallListener();
        TelephonyManager telManager = (TelephonyManager) this
                .getSystemService(Context.TELEPHONY_SERVICE);
        telManager.listen(phoneCallListener,
                PhoneStateListener.LISTEN_CALL_STATE);

    }
    public void Salir(View v)
    {
        finish();
    }

    public void ConfigurarAI()
    {
        final AIConfiguration config = new AIConfiguration("819d431dc64b46fe88086223054940da",
                AIConfiguration.SupportedLanguages.Spanish,
                AIConfiguration.RecognitionEngine.System);

        config.setRecognizerStartSound(getResources().openRawResourceFd(R.raw.test_start));
        config.setRecognizerStopSound(getResources().openRawResourceFd(R.raw.test_stop));
        config.setRecognizerCancelSound(getResources().openRawResourceFd(R.raw.test_cancel));

        emergencia.initialize(config);
        emergencia.setResultsListener(listener);
    }


    public void Llamar(String nombre) {
        nombre.toLowerCase();
        nombre= Normalizer.normalize(nombre, Normalizer.Form.NFD);
        nombre= nombre.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        if (nombre != null) {
            Toast.makeText(this, nombre, Toast.LENGTH_SHORT).show();
            Contact c = mydb.getDatabyName(nombre);
            if (c != null) {
                if(c.phone!=null) {
                    Toast.makeText(this, c.phone, Toast.LENGTH_SHORT).show();
                    call(c.phone);

                    sendSmsByManager(c.phone);
                }else Toast.makeText(this,"ERROR SIN NÚMERO", Toast.LENGTH_SHORT).show();

            }else  Toast.makeText(this,"ERROR CONTACTO NO ENCONTRADO", Toast.LENGTH_SHORT).show();
        }

    }

    public void LlamaraFavorito1(View v)
    {
        clickContacto=true;
        String co1= sharedPreference.getFavorito1(this.getApplicationContext());
        if(co1!=null)
        {
            Llamar(co1);
        }

    }

    public void LlamaraFavorito2(View v)
    {
        clickContacto=true;
        String co1= sharedPreference.getFavorito2(this.getApplicationContext());
         if(co1!=null)
         {
             Llamar(co1);
         }
    }


    public void LlamaraFavorito3(View v)
    {
        clickContacto=true;
        String co3= sharedPreference.getFavorito1(this.getApplicationContext());
        if(co3!=null)
        {
            Llamar(co3);
        }
    }

    private void call(String number) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setPackage("com.android.server.telecom");
        intent.setData(Uri.parse("tel:" + number));


        try {
            startActivity(intent);
        } catch (Exception E) {
            Toast.makeText(this, "no pude llamar", Toast.LENGTH_SHORT).show();
            System.out.println(E.toString());
        }
    }


    public void MensajeInicio(){

        mMediaPlayer.start();

        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                     emergencia.startListening();

            }
        });
    }
    public void leerFavoritos(){

        if(sharedPreference.esVacio(this))
        {
            Toast.makeText(this,"Sin Contactos Favoritos",Toast.LENGTH_SHORT).show();
        }else
        {
            String co1= sharedPreference.getFavorito1(this.getApplicationContext());
            String co2= sharedPreference.getFavorito2(this.getApplicationContext());
            String co3= sharedPreference.getFavorito3(this.getApplicationContext());


            if(co1!=null) {

                LlenarFavorito(co1,im1);
            }

            if(co2!=null) {

                LlenarFavorito(co2,im2);
            }
            if(co3!=null)
            {
                LlenarFavorito(co3,im3);
            }

        }
    }

    void LlenarFavorito(String name, ImageView P) {

        if (name != null && P != null ) {
            Contact c = mydb.getDatabyName(name);
            Uri r = null;

            if (c != null) {
                if (c.photo != null) {
                    r = Uri.parse(c.photo);
                }

                //  imagenCoche.setImageURI(r);
                if (r != null) {
                    //  Picasso.with(context).load(r).into(imagenCoche);
                    Glide.with(this.getApplicationContext())
                            .load(Uri.parse(c.photo)).into(P);
                }


            }
        }
    }
    /**
     * Arranca los servicios de google de ubicación
     */
    void ArrancarServiciosdeUbicacion()
    {

        // Kick off the process of building the GoogleApiClient, LocationRequest, and
        // LocationSettingsRequest objects.
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
    }
    /**
     * Updates fields based on data stored in the bundle.
     *
     * @param savedInstanceState The activity state saved in the Bundle.
     */
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
            updateUI();
        }
    }

    /**
     * Builds a GoogleApiClient
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }


    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(Auxilio.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }


    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = true;
            }
        });

    }

    /**
     * Updates all UI fields.
     */
    private void updateUI() {
        updateLocationUI();
    }



    /**
     * Sets the value of the UI fields for the location latitude, longitude and last update time.
     */
    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            /*mLatitudeTextView.setText(String.format("%s: %f", mLatitudeLabel,
                    mCurrentLocation.getLatitude()));
            mLongitudeTextView.setText(String.format("%s: %f", mLongitudeLabel,
                    mCurrentLocation.getLongitude()));
            mLastUpdateTimeTextView.setText(String.format("%s: %s", mLastUpdateTimeLabel,
                    mLastUpdateTime));*/
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
         emergencia.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
       emergencia.pause();

    }

    @Override
    protected void onStop() {
        super.onStop();
        stopLocationUpdates();
        mGoogleApiClient.disconnect();

    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Connected to GoogleApiClient");


        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                    (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
            updateLocationUI();
        }

        mResultReceiver = new AddressResultReceiver(null);
        if (mGoogleApiClient.isConnected() && mCurrentLocation != null) {
            startIntentService();
        }
    }
    protected void startIntentService() {
        Intent intent = new Intent(this, ObtenerDireccionIntentService.class);

        intent.putExtra(Constants.RECEIVER, mResultReceiver);

        intent.putExtra(Constants.LOCATION_DATA_EXTRA, mCurrentLocation);
        intent.putExtra(Constants.LOCATION_LATITUDE_DATA_EXTRA,mCurrentLocation.getLatitude());
        intent.putExtra(Constants.LOCATION_LONGITUDE_DATA_EXTRA,mCurrentLocation.getLongitude());
        startService(intent);
    }
    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

        startIntentService() ;
        updateLocationUI();
        //Toast.makeText(this,"Location updated",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }
    @SuppressLint("ParcelCreator")
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string
            // or an error message sent from the intent service.
            final String mAddressOutput = resultData.getString(Constants.RESULT_DATA_KEY);
            final Address address = resultData.getParcelable(Constants.RESULT_ADDRESS);

            // Show a toast message if an address was found.
            if (resultCode == Constants.SUCCESS_RESULT) {
                System.out.println(resultData.getString(Constants.RESULT_DATA_KEY));
                System.out.println(mAddressOutput);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dirrecionResuelta=mAddressOutput;
                    }
                });
                //MostrarToast(resultData.getString(Constants.RESULT_DATA_KEY));
            }

        }
    }

        public void sendSmsByManager(String telefono) {

            try {

                // Get the default instance of the SmsManager
                System.out.println(telefono);

                SmsManager smsManager = SmsManager.getDefault();
                System.out.println("------" +dirrecionResuelta);
                TelephonyManager tMgr =(TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
                String mPhoneNumber = tMgr.getLine1Number();
               smsManager.sendTextMessage(telefono,null, "Ponte en contaco,Necesito ayuda:" + dirrecionResuelta, null, null);

                Toast.makeText(getApplicationContext(), "Your sms has successfully sent!" + telefono,
                        Toast.LENGTH_LONG).show();

            } catch (Exception ex) {

                Toast.makeText(getApplicationContext(),"Your sms has failed...",
                        Toast.LENGTH_LONG).show();

                ex.printStackTrace();

            }

        }


    void MostrarToast(String a)
    {
        Toast.makeText(this,a,Toast.LENGTH_SHORT).show();

    }
    public void superAlarma()
    {

        String co1= sharedPreference.getFavorito1(this.getApplicationContext());
        String co2= sharedPreference.getFavorito2(this.getApplicationContext());
        String co3= sharedPreference.getFavorito3(this.getApplicationContext());
        ArrayList<String> string= new ArrayList();
        string.add(co1);
        string.add(co2);
        string.add(co3);

        MostrarToast("PROTOCOLO MÁXIMA EMERGENCIA");
        for(int i=0;i<3;i++) {
            String co= string.get(i);
            co.toLowerCase();
            co = Normalizer.normalize(co, Normalizer.Form.NFD);
            co= co.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
            if (co != null) {
                Contact c = mydb.getDatabyName(co);
                if (c != null) {
                    if (c.phone != null) {
                        Toast.makeText(this, "Enviando mensaje a:"+ c.phone, Toast.LENGTH_SHORT).show();
                        sendSmsByManager(c.phone);
                    }
                }
            }
        }
    }
    public void esperar5segundos(int milisegundos) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // acciones que se ejecutan tras los milisegundos
                if(clickContacto==false) {
                    if(veces<3) {
                        mMediaPlayer.start();
                        MostrarToast("ESTAS BIEN? DI EL NOMBRE DE CONTACTO POR FAVOR" + veces);

                        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                emergencia.startListening();

                            }
                        });

                    }else
                    {
                        superAlarma();
                    }
                }
            }
        }, milisegundos);
    }

    private AIButton.AIButtonListener listener= new AIButton.AIButtonListener()
    {
        @Override
        public void onResult(final AIResponse response) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "onResult");

                    //resultTextView.setText(gson.toJson(response));

                    Log.i(TAG, "Received success response");

                    // this is example how to get different parts of result objectº
                    final ai.api.model.Status status = response.getStatus();
                    Log.i(TAG, "Status code: " + status.getCode());
                    Log.i(TAG, "Status type: " + status.getErrorType());

                    final Result result = response.getResult();
                    Log.i(TAG, "Resolved query: " + result.getResolvedQuery());

                    //resultTextView.setText(result.getResolvedQuery());

                    //  Log.i(TAG, "Action: " + result.getAction());
                    //    final String speech = result.getFulfillment().getSpeech();
                    //    Log.i(TAG, "Speech: " + speech);
                    //   TTS.speak(speech);

                    final Metadata metadata = result.getMetadata();
                    if (metadata != null) {
                        Log.i(TAG, "Intent id: " + metadata.getIntentId());
                        Log.i(TAG, "Intent name: " + metadata.getIntentName());
                    }

                    final HashMap<String, JsonElement> params = result.getParameters();
                    if (params != null && !params.isEmpty()) {
                        Log.i(TAG, "Parameters: ");
                        for (final Map.Entry<String, JsonElement> entry : params.entrySet()) {
                            Log.i(TAG, String.format("%s: %s", entry.getKey(), entry.getValue().toString()));
                        }
                    }


                    if(status.getCode()==200)
                    {

                        Llamar(result.getResolvedQuery());

                    }
                }
            });
        }
        @Override
        public void onError(final AIError error) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "onError");
                }
            });
            esperar5segundos(6000);
            veces++;
        }

        @Override
        public void onCancelled() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "onCancelled");
                }
            });
        }
    };

    private class PhoneCallListener extends PhoneStateListener {

        String TAG = "LOGGING PHONE CALL";

        private boolean phoneCalling = false;

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (TelephonyManager.CALL_STATE_RINGING == state) {
                // phone ringing
                Log.i(TAG, "RINGING, number: " + incomingNumber);
            }

            if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                // active
                Log.i(TAG, "OFFHOOK");

                Log.i(TAG, "------------------no responde++++++++++++++++++++++++++");
                phoneCalling = true;
            }

            // When the call ends launch the main activity again
            if (TelephonyManager.CALL_STATE_IDLE == state) {


                if (phoneCalling) {

                    Log.i(TAG, "restart app");
                    //getCallDetails();
                    // restart app
              /*  Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage(
                                getBaseContext().getPackageName());

                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);*/
                    phoneCalling = false;
                    System.out.println("Terminado");
                    if(!isFinishing()){

                        openPregunta();
                    }
                }

            }
        }
    }



    @Override
    protected void onDestroy() {
        mMediaPlayer.stop();
        super.onDestroy();
    }




}