package sanchez.gina.aliapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.transition.CircularPropagation;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by GinaMariela on 30/08/2016.
 */
public class DBContactHelper  extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Contact.db";
    public static final String CONTACTS_TABLE_NAME = "contacts";
    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_NAME = "name";
    public static final String CONTACTS_COLUMN_PHONE = "phone";
    public static final String CONTACTS_COLUMN_EMAIL = "email";
    public static final String CONTACTS_COLUMN_PHOTO = "photo";
    public static final String CONTACTS_COLUMN_ADDRESS = "address";

    private HashMap hp;

    public DBContactHelper (Context context)
    {

        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table contacts " + "(id integer primary key, name text,phone text,email text,photo text, address text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //hacer algo mejor
        db.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(db);
    }

    public boolean insertContact  (Contact contacto)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("name", contacto.name);
        contentValues.put("phone", contacto.phone);
        contentValues.put("email", contacto.email);
        contentValues.put("photo", contacto.photo);
        contentValues.put("address", contacto.address);
        long c= db.insert("contacts", null, contentValues);

        return true;
    }
    public Contact getDatabyName(String nombres){
        SQLiteDatabase db = this.getReadableDatabase();
        System.out.println("llamanado contactos by name");
        Cursor res =  db.rawQuery( "select * from contacts where name like '%"+nombres+"%'", null );
        if(res!=null) {
            if( res.moveToFirst()) {
                System.out.println("---------------" + res.getCount() + "---------------");
                int idd = res.getInt(res.getColumnIndex(CONTACTS_COLUMN_ID));

                String nombre = res.getString(res.getColumnIndex(CONTACTS_COLUMN_NAME));
                String telefono = res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHONE));
                String photo = res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHOTO));
                String direccion = res.getString(res.getColumnIndex(CONTACTS_COLUMN_ADDRESS));
                String email = res.getString(res.getColumnIndex(CONTACTS_COLUMN_EMAIL));
                Contact c = new Contact(idd, nombre, telefono, email, photo, direccion);

                return c;
            }
        }
        else System.out.println("No existe"); return null;
    }
    public Contact getData(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from contacts where id="+id+"", null );
        res.moveToFirst();
        System.out.println("---------------"+res.getCount()+"---------------");
        int idd= res.getInt(res.getColumnIndex(CONTACTS_COLUMN_ID));
// OJO MIRAR PUNTERO RES SI ES NULO QUE HACER..
        String nombre= res.getString(res.getColumnIndex(CONTACTS_COLUMN_NAME));
        String telefono= res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHONE));
        String photo= res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHOTO));
        String direccion= res.getString(res.getColumnIndex(CONTACTS_COLUMN_ADDRESS));
        String email= res.getString(res.getColumnIndex(CONTACTS_COLUMN_EMAIL));
        Contact c= new Contact(id,nombre,telefono,email,photo,direccion);

        return c;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, CONTACTS_TABLE_NAME);
        return numRows;
    }

    public boolean updateContact (Integer id, String name, String phone, String email, String photo,String address)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("email", email);
        contentValues.put("photo", photo);
        contentValues.put("address",address);
        db.update( CONTACTS_TABLE_NAME , contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Integer deleteContact (Integer id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete( CONTACTS_TABLE_NAME ,
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

    public ArrayList<Contact> getAllContacts()
    {

            ArrayList<String> array_list = new ArrayList<String>();

            ArrayList<Contact> misContactos= new  ArrayList<Contact>();
            //hp = new HashMap();
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor res =  db.rawQuery( "select * from "+CONTACTS_TABLE_NAME , null );
            res.moveToFirst();

            while(res.isAfterLast() == false){
             //   array_list.add(res.getString(res.getColumnIndex(CONTACTS_COLUMN_NAME)));
                int id= res.getInt(res.getColumnIndex(CONTACTS_COLUMN_ID));
                String nombre= res.getString(res.getColumnIndex(CONTACTS_COLUMN_NAME));
                String telefono= res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHONE));
                String photo= res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHOTO));
                String direccion= res.getString(res.getColumnIndex(CONTACTS_COLUMN_ADDRESS));
                String email= res.getString(res.getColumnIndex(CONTACTS_COLUMN_EMAIL));
                Contact c= new Contact(id,nombre,telefono,email,photo,direccion);
                misContactos.add(c);
                res.moveToNext();
            }
            return misContactos;
    }
}

