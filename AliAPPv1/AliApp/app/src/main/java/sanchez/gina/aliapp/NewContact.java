package sanchez.gina.aliapp;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ai.api.ui.AIButton;

public class NewContact extends AppCompatActivity {
    DBContactHelper mydb ;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    String nombrep;
    Button save;
    Button photo;
    EditText nombre;
    EditText direccion;
    EditText telefono;
    EditText email;
    ImageView pic;
    static final int REQUEST_TAKE_PHOTO = 1;
    String mCurrentPhotoPath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        mydb = new DBContactHelper(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_contact);


        photo = (Button) findViewById(R.id.tomarfoto);
        pic = (ImageView) findViewById(R.id.photome);
        save = (Button) findViewById(R.id.buttonsave);
        nombre= (EditText) findViewById(R.id.editTextName);
        telefono= (EditText)findViewById(R.id.editTextPhone);
        email= (EditText)findViewById(R.id.editTextEmail);
        direccion= (EditText) findViewById(R.id.editTextDireccion);



        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {



                //OJO COMPROBAR SI MCURRENTPATH ESTA VACIO?

                System.out.println("creando ************************");
                System.out.println(nombre.getText().toString());
                System.out.println(telefono.getText().toString());
                System.out.println(mCurrentPhotoPath);
                Contact c= new Contact(0,nombre.getText().toString(),telefono.getText().toString(),email.getText().toString(),mCurrentPhotoPath,direccion.getText().toString());
                mydb.insertContact(c);

                Intent intent = new Intent(getApplicationContext(), DetailContact.class);

                // Poner el Id de la imagen como extra en la intención
                intent.putExtra("idContacto", c);

                // Aquí pasaremos el parámetro de la intención creada previamente
                startActivity(intent);

            }
        });
        photo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarNuevoContacto);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
    }
    /*@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }*/
    private File createImageFile() throws IOException {


        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        //File storageDir = getApplicationContext().getFilesDir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        nombrep= image.getName();
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                System.out.println("error no creado");
                System.out.println(ex.toString());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "sanchez.gina.aliapp",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }


    public void delelePhoto(View v)
    {
      //  Uri p=Uri.parse(mCurrentPhotoPath);
        System.out.println("*******"+nombrep+"**************");

        File dir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File file = new File(dir, nombrep);
        boolean deleted = file.delete();
      /* ;*/


        System.out.println(deleted);
        /*
        if(newFile!=null) {
            boolean eliminado= file.delete();
            System.out.println(mCurrentPhotoPath);
            //this.getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(mCurrentPhotoPath))));
        }*/

    }
    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

      if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
          if(mCurrentPhotoPath!=null) {
              Uri r = Uri.parse(mCurrentPhotoPath);

              //  imagenCoche.setImageURI(r);
              if (r != null) {
                  Glide.with(this.getApplicationContext())
                          .load(Uri.parse(mCurrentPhotoPath)).into(pic);
              }
          }

      }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if(mCurrentPhotoPath!=null) {
            savedInstanceState.putString("filePath", mCurrentPhotoPath);

            /*BitmapDrawable drawable = (BitmapDrawable) pic.getDrawable();
            if (drawable != null) {
                Bitmap bitmap = drawable.getBitmap();
                savedInstanceState.putParcelable("image", bitmap);
            }*/
        }
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);

        Log.d("nuevo contacto++", "onSaveStateBundle() called");
    }


    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if(savedInstanceState != null) {
            mCurrentPhotoPath= savedInstanceState.getString("filePath");

            if(mCurrentPhotoPath!=null) {
                Glide.with(this.getApplicationContext())
                        .load(Uri.parse(mCurrentPhotoPath)).into(pic);
         /*   Bitmap bitmap = savedInstanceState.getParcelable("image");
            pic.setImageBitmap(bitmap);*/
            }
        }
        Log.d("nuevo contacto++", "onRestortInstanceStateBundle() called");
    }

}
