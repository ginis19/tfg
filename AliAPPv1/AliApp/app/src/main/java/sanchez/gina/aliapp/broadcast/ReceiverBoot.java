package sanchez.gina.aliapp.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import sanchez.gina.aliapp.servicios.SensorServiceProximity;

public class ReceiverBoot extends BroadcastReceiver {
    public ReceiverBoot() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case Intent.ACTION_BOOT_COMPLETED:
                context.startService(new Intent(context, SensorServiceProximity.class));
            default:

        }
    }
}
