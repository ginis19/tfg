package sanchez.gina.aliapp;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
import com.bumptech.glide.Glide;
import com.google.android.gms.vision.text.Text;

import java.util.ArrayList;

import sanchez.gina.aliapp.Preferencias.SharedPreference;

public class FavoritosContactos extends AppCompatActivity   {
        DBContactHelper mydb;
    AlertDialog actions;
    TextView f1;
    TextView f2;
    TextView f3;

    BootstrapCircleThumbnail p1;

    BootstrapCircleThumbnail p2;
    BootstrapCircleThumbnail p3;
    private SharedPreference sharedPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoritos_contactos);
        sharedPreference = new SharedPreference();

        mydb = new DBContactHelper(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarFavoritos);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();


        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
        p1  = (BootstrapCircleThumbnail) findViewById(R.id.fav1);
        p2  = (BootstrapCircleThumbnail) findViewById(R.id.fav2);
        p3  = (BootstrapCircleThumbnail) findViewById(R.id.fav3);

        f1= (TextView)findViewById(R.id.NameFav1);
        f2= (TextView)findViewById(R.id.NameFav2);
        f3= (TextView)findViewById(R.id.NameFav3);

        leerFavoritos();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),EditFavorito.class);
                startActivity(intent);

            }
        });
    }

    public void leerFavoritos(){

        if(sharedPreference.esVacio(this))
        {
            Toast.makeText(this,"vacio",Toast.LENGTH_SHORT).show();
        }else
        {
            String co1= sharedPreference.getFavorito1(this.getApplicationContext());
            String co2= sharedPreference.getFavorito2(this.getApplicationContext());
            String co3= sharedPreference.getFavorito3(this.getApplicationContext());


            if(co1!=null) {

                LlenarFavorito(co1,p1,f1);
            }

            if(co2!=null) {

                LlenarFavorito(co2,p2,f2);
            }
            if(co3!=null)
            {
                LlenarFavorito(co3,p3,f3);
            }

        }
    }

    void LlenarFavorito(String name,  BootstrapCircleThumbnail P, TextView t) {

        if (name != null && P != null && t != null) {
            Contact c = mydb.getDatabyName(name);
            Uri r = null;

            if (c != null) {
                if (c.photo != null) {
                    r = Uri.parse(c.photo);
                }

                //  imagenCoche.setImageURI(r);
                if (r != null) {
                    //  Picasso.with(context).load(r).into(imagenCoche);
                    Glide.with(this.getApplicationContext())
                            .load(Uri.parse(c.photo)).into(P);
                }

                if (c.name != null) {
                    t.setText(c.name);
                }
            }
        }
    }

}
