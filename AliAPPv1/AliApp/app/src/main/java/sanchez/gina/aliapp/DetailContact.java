package sanchez.gina.aliapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailContact extends AppCompatActivity {
    DBContactHelper mydb ;
    TextView nombre;
    TextView direccion;
    TextView telefono;
    TextView email;
    ImageView imagen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_contact);

        mydb = new DBContactHelper(this);
        Intent i = getIntent();
        Contact c= (Contact) i.getExtras().getSerializable("idContacto");
        //System.out.println(FlagId);


        nombre= (TextView) findViewById(R.id.nombre);

        direccion = (TextView) findViewById(R.id.dircontact);

        telefono = (TextView) findViewById(R.id.telefonocontact);

        email = (TextView) findViewById(R.id.emailcontact);

        imagen= (ImageView) findViewById(R.id.fotocontact);
        nombre.setText(c.name);
        direccion.setText(c.address);
        telefono.setText(c.phone);
        email.setText(c.email);
        Uri r=null;
        if(c.photo!=null) {
            r= Uri.parse(c.photo);
        }

        //  imagenCoche.setImageURI(r);
        if( r!=null) {
            //  Picasso.with(context).load(r).into(imagenCoche);
            Glide.with(this.getApplicationContext())
                    .load(Uri.parse(c.photo)).into(imagen);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.detallecontacto);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        //llenarDatos(FlagId);
    }
    /*void llenarDatos(int id)
    {

       Contact c=  mydb.getData(id);

      nombre.setText(c.name);
        direccion.setText(c.address);
        telefono.setText(c.phone);
        email.setText(c.email);

    }*/
}
