package sanchez.gina.aliapp;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by GinaMariela on 30/08/2016.
 */
public class Contact  implements Serializable {
    public String name;
    int id;
    String phone;
    String email;
    String photo;
    String address;


    public Contact(int id,String name, String phone, String email, String photo, String adress) {
        this.id= id;
        this.name = name;
        this.email= email;
        this.photo=photo;
        this.address= adress;
        this.phone= phone;
    }


}
