package sanchez.gina.aliapp.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import sanchez.gina.aliapp.servicios.SensorServiceProximity;

public class SensorBroadcastReceiver extends BroadcastReceiver {
    public static boolean wasScreenOn = true;
    public SensorBroadcastReceiver() {
    }


    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        System.out.println("-------------"+ action);

        if(action.equals("RestartSensor")) {
            Log.i(SensorBroadcastReceiver.class.getSimpleName(), "Service Stops!!!!!");

            context.startService(new Intent(context, SensorServiceProximity.class));
        }


/*
        if(action.equals(Intent.ACTION_SCREEN_OFF))
        {
            wasScreenOn = false;

            System.out.println("------------------------");
            // Toast.makeText(context, "MyReceiver Started", Toast.LENGTH_SHORT).show();
            System.out.println("pantalla apagada");
            //    Intent myIntent = new Intent(context, MainActivity.class);
            //   myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //  context.startActivity(myIntent);

        }else if( intent.getAction().equals(Intent.ACTION_SCREEN_ON))
        {
            System.out.println("------------------------");
            wasScreenOn = true;
            System.out.println("pantalla encendida");
        }

*/
    }
}
